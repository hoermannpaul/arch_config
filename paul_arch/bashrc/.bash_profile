#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc
[[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]] && exec startx && exit;
[[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty2 ]] && exec startx --:2 && exit; 
[[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty3 ]] && exec startx --:3 && exit;

#
# Aliases
#

# utility
alias zshrc="source ~/.zshrc"

# docker
alias dsa="docker ps -q | docker stop"
alias dsp="docker system prune"
alias dcs="docker-compose --env-file ./.env --log-level ERROR -f services/docker-compose.yml"
alias dca="docker-compose --env-file ./.env --log-level ERROR -f services/docker-compose.yml -f components/docker-compose.yml"
alias dcc="docker-compose --env-file ./.env --log-level ERROR -f components/docker-compose.yml"
alias dlog="docker logs"
alias drmf="docker rm -f"
alias dps="docker ps --format='table {{.ID}}\t{{.Image}}\t{{.Status}}\t{{.Names}}'"

xalldir() {
	ls -l | grep "^d" | tr -s " " | cut -d" " -f 9 | xargs -I {} bash -c "cd {} && $*"
}
alias drma="docker rm -f '$(docker ps -aq)'"

drmi() {
	docker image rm @0 $(docker image ls -q)
}

drmv() {
	docker volume rm @0 $(docker volume ls -q)
}

# poetry
alias pinst="poetry install --all-extras"
alias pupd="poetry update"
alias ptox="poetry run tox -e format,lint"

# python

penv() {
	[ -d "./venv/" ] && source venv/bin/activate || \
	[ -d "./.venv/" ] && source .venv/bin/activate
}

dshell() {
	docker exec -it $1 sh
}

drestart() {
	docker restart $1 $1-sidecar
}

gitsmb() {
	git submodule | grep $1 | cut -d\  -f3
}

# git
alias gits="git status"
alias gitls="git log -10 --pretty=oneline"
alias gitpb="git push --set-upstream origin"

gitcm() {
	MESSAGE=\"$*\"
	git commit -m $MESSAGE
}


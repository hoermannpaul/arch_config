#!/bin/fish

while true

	set BAT (acpi | awk '{printf $4 " " $5}' | sed s/,//g)
	set RAM (free | grep Mem | awk '{printf "%5.1f%", $3/$2 * 100}')
	set DATE (date)
	set ICONSPACE "           "

	set CONTENT (printf " [ BAT: %s | RAM: %s | %s ] %s " $BAT $RAM $DATE $ICONSPACE)

	xsetroot -name $CONTENT
	sleep 1

end

#!/bin/bash
DEFAULT_INNER=$(cat ~/.config/i3/config | grep 'set \$gap_inner_default' | cut -d' ' -f3)
DEFAULT_OUTER=$(cat ~/.config/i3/config | grep 'set \$gap_outer_default' | cut -d' ' -f3)
CURRENT_INNER=$(i3-msg -t get_tree | grep -Po '\"gaps\":{\"inner\":(-|)[0-9]+(?=.*\"focused\":true)' | tail -1 | cut -d: -f3)
CURRENT_OUTER=$(i3-msg -t get_tree | grep -Po '\"gaps\":{\"inner\":(-|)[0-9]*,\"outer\":(-|)[0-9]+(?=.*\"focused\":true)' | tail -1 | cut -d: -f4)
PREV_INNER=$(cat ~/.scripts/i3.db | head -1)
PREV_OUTER=$(cat ~/.scripts/i3.db | tail -1)
echo $CURRENT_INNER
echo $CURRENT_OUTER
if [[ -z $PREV_INNER || $PREV_INNER == 0 ]]; then
	PREV_INNER=${CURRENT_INNER#-}
	echo 'set'
fi
if [[ -z $PREV_OUTER || $PREV_OUTER == 0 ]]; then
	PREV_OUTER=${CURRENT_OUTER#-}
	echo 'set'
fi
echo $PREV_INNER
echo $PREV_OUTER
if [[ $CURRENT_INNER != -$DEFAULT_INNER || $CURRENT_OUTER != -$DEFAULT_OUTER ]]; then
	echo "$(($PREV_INNER+$DEFAULT_INNER))" > ~/.scripts/i3.db
	echo "$(($PREV_OUTER+$DEFAULT_OUTER))" >> ~/.scripts/i3.db
	i3-msg gaps inner current set 0
	i3-msg gaps outer current set 0 
else
	i3-msg gaps inner current set $PREV_INNER
	i3-msg gaps outer current set $PREV_OUTER
	echo "0" > ~/.scripts/i3.db
	echo "0" >> ~/.scripts/i3.db
fi


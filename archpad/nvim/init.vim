syntax enable
set nu rnu

let g:python_host_prog = '/usr/bin/python'

" Disable Mouse
set mouse=r
"set omnifunc=htmlcomplete#CompleteTags
"set omnifunc=csscomplete#CompleteCSS
"au BufNewFile, BufRead *.js set filetype=javascript 
" Plugins
call plug#begin('~/.vim/plugged')
Plug 'tmhedberg/SimpylFold'
Plug 'schickele/vim'
Plug 'alvan/vim-closetag'
Plug 'pangloss/vim-javascript'
Plug 'vim-scripts/L9'
Plug 'Valloric/YouCompleteMe'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tpope/vim-fugitive'
"Plug 'scrooloose/syntastic'
Plug 'tpope/vim-surround'
Plug 'kien/ctrlp.vim'
Plug 'altercation/vim-colors-solarized'
Plug 'tpope/vim-surround'
Plug 'vim-scripts/FuzzyFinder'
Plug 'mattn/emmet-vim'
Plug 'Chiel92/vim-autoformat'
"Plug 'skammer/vim-css-color'
Plug 'hail2u/vim-css3-syntax'
"Plug 'kchmck/vim-coffee-script'
Plug 'digitaltoad/vim-pug'
Plug 'gorodinskiy/vim-coloresque'
Plug 'sheerun/vim-polyglot'
Plug 'itchyny/lightline.vim'
"Plug 'garbas/vim-snipmate'
Plug 'tomtom/tlib_vim'
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'honza/vim-snippets'
Plug 'mxw/vim-jsx'
Plug 'jiangmiao/auto-pairs'
Plug 'SirVer/ultisnips'
Plug 'ervandew/supertab'
Plug 'drewtempelmeyer/palenight.vim'
Plug 'ayu-theme/ayu-vim'
Plug 'morhetz/gruvbox'
Plug 'joshdick/onedark.vim'
Plug 'mhartington/oceanic-next'
Plug 'arcticicestudio/nord-vim'
Plug 'mtscout6/syntastic-local-eslint.vim'
"Plug 'sbdchd/neoformat'
"Plug 'heavenshell/vim-tslint-config'
Plug 'prettier/vim-prettier', {
    \ 'do': 'npm install',
    \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss'] }
Plug 'mtscout6/syntastic-local-eslint.vim'
Plug 'w0rp/ale'
Plug 'Quramy/tsuquyomi'
Plug 'Shougo/vimproc.vim'
"Plug 'osyo-manga/vim-watchdogs'
"Plug 'thinca/vim-quickrun'
"Plug 'Shougo/vimproc.vim'
"Plug 'osyo-manga/shabadou.vim'
"Plug 'jceb/vim-hier'
"Plug 'dannyob/quickfixstatus'
Plug 'othree/xml.vim'
Plug 'aradunovic/perun.vim'
call plug#end()

function! StrTrim(txt)
  return substitute(a:txt, '^\n*\s*\(.\{-}\)\n*\s*$', '\1', '')
endfunction

let g:ale_linters = {
\   'javascript': ['eslint'],
\   'typescript': ['tslint'],
\}

let g:ale_fixers = {
\   'javascript': ['eslint'],
\   'typescript': ['tslint'],
\}

"TODO: If Filetype not supported by ALE, enable Syntastic instead
" Syntastic
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 0
"let g:syntastic_check_on_wq = 1
"let b:syntastic_javascript_eslint_exec = StrTrim(system('npm-which eslint'))
"let g:syntastic_javascript_checkers = ['eslint']
" tslint
"let g:tslint_configs = [
"  \ 'tslint-config-standard',
"  \ 'tslint.json'
"  \ ]
"autocmd VimEnter * QuickfixStatusEnable
"autocmd VimEnter * QuickRun
"let g:quickrun#config['javascript/watchdogs_checker'] = {
"  \ 'type': 'watchdogs_checker/tslint',
"  \ 'hook/watchdogs_quickrun_running_tslint/enable': 1,
"  \ }

" Theme
set termguicolors
let ayucolor="light"
"let ayucolor="mirage"
"let ayucolor="mirage"
colorscheme fruchtig
set background=light
"colorscheme perun
set termguicolors
"set background=dark

" Unspecified
autocmd BufNewFile,BufRead *.inc set filetype=html
let g:javascript_plugin_flow = 1
let g:vim_jsx_pretty_enable_jsx_highlight = 1
let g:vim_jsx_pretty_colorful_config = 1
"let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_typescript_checkers = ['tslint', 'tsc']
let g:syntastic_typescript_checkers = ['tsuquyomi']
if executable('node_modules/.bin/tslint')
  let b:syntastic_typescript_tslint_exec = 'node_modules/.bin/tslint'
endif
let g:jsx_ext_required = 1
let g:user_emmet_install_clobal = 0
let g:cssColorVimDoNotMessMyUpdatetime = 1
" Identation
set tabstop=3
set shiftwidth=3
set smartindent
set smarttab
"set softtabstop=0 noexpandtab

" YouCompleteMe
let g:ycm_global_ycm_extra_conf = '~/git/fhhgb_ws17/swo/.ycm_extra_conf.py'
"imap <C-S-l> <Plug>snipMateNextOrTrigger
"smap <C-S-l> <Plug>snipMateNextOrTrigger
"imap <C-S-h> <Plug>snipMateBack
" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
"let g:ycm_key_list_select_completion = '<C-S>'
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']

" Tabstuff
let g:SuperTabDefaultCompletionType = '<C-n>'
" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

" enable NERDtree on startup
let g:nerdtree_tabs_open_on_gui_startup = 1
let g:nerdtree_tabs_open_on_console_startup = 1
" autocmd VimEnter * NERDTree
" autocmd BufWritePre *.js Neoformat

set spell spelllang=en_us

set foldmethod=syntax
set foldlevelstart=5

let g:lightline = {
			\ 'colorscheme': 'solarized',
			\}

" gvim only options
if has("gui_running")
    set guifont=FantasqueSansMono\ Nerd\ Font\ Mono\ 12
endif

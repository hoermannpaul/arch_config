#!/bin/bash
DEFAULT_INNER=$(cat ~/.config/i3/config | grep 'set \$gap_inner_default' | cut -d' ' -f3)
DEFAULT_OUTER=$(cat ~/.config/i3/config | grep 'set \$gap_outer_default' | cut -d' ' -f3)
i3-msg gaps inner current set $DEFAULT_INNER
i3-msg gaps outer current set $DEFAULT_OUTER
echo "0" > ~/.scripts/i3.db
echo "0" >> ~/.scripts/i3.db


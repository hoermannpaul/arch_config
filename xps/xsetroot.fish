#!/bin/fish

while true

	set BAT (acpi | awk '{printf $4 " " $5}')
	set RAM (free | grep Mem | awk '{printf "%5.1f%", $3/$2 * 100}')
	set CPU (mpstat -P ALL | grep all | awk '{printf "%5.1f%", 100-$13}')
	set DATE (date)
	set ICONSPACE "           "

	set CONTENT (printf " [ BAT: %s | CPU: %s | RAM: %s | %s ] %s " $BAT $CPU $RAM $DATE $ICONSPACE)

	xsetroot -name $CONTENT
	sleep 1

end
